package echo

import (
	"time"
)

func CurrentTime() time.Time {
	return time.Now()
}

func CurrentTimeAtTimeZone(timezone string) time.Time {
	location, err := time.LoadLocation(timezone)
	if err != nil {
		return CurrentTime()
	}
	return time.Now().In(location)
}
